# Currently the directory is based on Red Hat Linux
# But other distro supports are also welcome to commit here
#

RPM_BUILD_DIR=/usr/src/redhat/BUILD/Canna-3.6/Canna36p1
POD=$(RPM_BUILD_DIR)/dic/ideo/pubdic/pod
PKG=pubdic-bonobo-`date '+%Y%m%d'`.tar.bz2

all: $(PKG)

pubdic-bonobo/bonobo.p:
	rm -rf pubdic-bonobo-*.tar.bz2
	rm -rf pubdic-bonobo
	mkdir -p pubdic-bonobo
	cat *.p | sort > pubdic-bonobo/bonobo.p
	cp README pubdic-bonobo/

$(PKG): pubdic-bonobo/bonobo.p
	tar cjvf $(PKG) pubdic-bonobo

clean:
	rm -rf pubdic-bonobo
	rm -rf *.tar.bz2
